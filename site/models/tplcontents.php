<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Itemlist
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2021 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Helper\TagsHelper;
use \Joomla\CMS\Layout\FileLayout;
use \Joomla\Utilities\ArrayHelper;


/**
 * Methods supporting a list of Itemlist records.
 *
 * @since  1.6
 */
class ItemlistModelTplcontents extends \Joomla\CMS\MVC\Model\ListModel
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'ordering', 'a.ordering',
				'state', 'a.state',
				'created_by', 'a.created_by',
				'modified_by', 'a.modified_by',
				'name', 'a.name',
				'content', 'a.content',
			);
		}

		parent::__construct($config);
	}

        
        
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app  = Factory::getApplication();
            
		$list = $app->getUserState($this->context . '.list');

		$ordering  = isset($list['filter_order'])     ? $list['filter_order']     : null;
		$direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;
		if(empty($ordering)){
		$ordering = $app->getUserStateFromRequest($this->context . '.filter_order', 'filter_order', $app->get('filter_order'));
		if (!in_array($ordering, $this->filter_fields))
		{
		$ordering = "a.id";
		}
		$this->setState('list.ordering', $ordering);
		}
		if(empty($direction))
		{
		$direction = $app->getUserStateFromRequest($this->context . '.filter_order_Dir', 'filter_order_Dir', $app->get('filter_order_Dir'));
		if (!in_array(strtoupper($direction), array('ASC', 'DESC', '')))
		{
		$direction = "ASC";
		}
		$this->setState('list.direction', $direction);
		}

		$list['limit']     = $app->getUserStateFromRequest($this->context . '.list.limit', 'limit', $app->get('list_limit'), 'uint');
		$list['start']     = $app->input->getInt('start', 0);
		$list['ordering']  = $ordering;
		$list['direction'] = $direction;

		$app->setUserState($this->context . '.list', $list);
		$app->input->set('list', null);
           
            
        // List state information.

        parent::populateState($ordering, $direction);

        $context = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
        $this->setState('filter.search', $context);

        

        // Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

        // Split context into component and optional section
        $parts = FieldsHelper::extract($context);

        if ($parts)
        {
            $this->setState('filter.component', $parts[0]);
            $this->setState('filter.section', $parts[1]);
        }
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
            // Create a new query object.
            $db    = $this->getDbo();
            $query = $db->getQuery(true);

            // Select the required fields from the table.
            $query->select(
                        $this->getState(
                                'list.select', 'DISTINCT a.*'
                        )
                );

            $query->from('`#__ls_tplcontent` AS a');
            
		// Join over the users for the checked out user.
		$query->select('uc.name AS uEditor');
		$query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		// Join over the created by field 'created_by'
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');

		// Join over the created by field 'modified_by'
		$query->join('LEFT', '#__users AS modified_by ON modified_by.id = a.modified_by');
            
		if (!Factory::getUser()->authorise('core.edit', 'com_itemlist'))
		{
			$query->where('a.state = 1');
		}
		else
		{
			$query->where('(a.state IN (0, 1))');
		}

            // Filter by search in title
            $search = $this->getState('filter.search');

            if (!empty($search))
            {
                if (stripos($search, 'id:') === 0)
                {
                    $query->where('a.id = ' . (int) substr($search, 3));
                }
                else
                {
                    $search = $db->Quote('%' . $db->escape($search, true) . '%');
                }
            }
            

            
            
            // Add the list ordering clause.
            $orderCol  = $this->state->get('list.ordering', "a.id");
            $orderDirn = $this->state->get('list.direction', "ASC");

            if ($orderCol && $orderDirn)
            {
                $query->order($db->escape($orderCol . ' ' . $orderDirn));
            }

            return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
			$app  = Factory::getApplication();
			$bedrooms=$app->input->get('Bedrooms','all');
			$neighbourhood=$app->input->get('neighbourhood','all');
			$minPrice=$app->input->get('min',0);
			$maxPrice=$app->input->get('max',100000000);
			$sort=$app->input->get('sort','all');
			$limit=$app->input->get('_limit',12);

			$query='_limit='.$limit;

			if($bedrooms != "all"){
				$query=$query.'&Bedrooms='.$bedrooms;
			}
			if($neighbourhood != "all"){
				$query=$query.'&neighbourhood='.$neighbourhood;
			}



		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://carolineolds-strapi-dev.q.starberry.com/properties?'.$query,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_POSTFIELDS => array('key' => 'zQIziBlBxJIUlK8PgEnHFOdY6ifbyvBZg6N7uditaISnxPB2Fr','resource' => 'products_list','limit' => '100'),
		  CURLOPT_HTTPHEADER => array(
		    'AuthorizationKey: zQIziBlBxJIUlK8PgEnHFOdY6ifbyvBZg6N7uditaISnxPB2F'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		
		$raw=json_decode($response);

		$data['query']['bedrooms']=$bedrooms;
		$data['query']['neighbourhood']=$neighbourhood;
		$data['query']['minPrice']=$minPrice;
		$data['query']['maxPrice']=$maxPrice;
		$data['query']['sort']=$sort;	

		$data['bedrooms']['all']= "All Bedrooms";
		$data['bedrooms']['1']= "1 Bedrooms";
		$data['bedrooms']['2']= "2 Bedrooms";
		$data['bedrooms']['3']= "3 Bedrooms";
		$data['bedrooms']['4']= "4 Bedrooms";
		$data['bedrooms']['5']= "5 Bedrooms";
		

		$data['Neighbourhoods']['all']= "Any Neighbourhoods";
	

		$data['minPrice']["0"]="Min Price";
		$data['minPrice']["1000"]="1000";
		$data['minPrice']["2000"]="2000";
		$data['minPrice']["3000"]="3000";
		$data['minPrice']["5000"]="5000";
		$data['minPrice']["7000"]="7000";
		$data['minPrice']["10000"]="10000";



		$data['maxPrice']["1000000000"]="Max Price";
		$data['maxPrice']["10000"]="10000";
		$data['maxPrice']["20000"]="20000";
		$data['maxPrice']["50000"]="50000";
		$data['maxPrice']["75000"]="75000";
		$data['maxPrice']["100000"]="100000 ";
		$data['maxPrice']["100000000"]="above";

		$data['items']=[];

		foreach($raw as $item){
		
			// if(isset($item->Bedrooms)){
			// 	$data['bedrooms'][$item->Bedrooms]=$item->Bedrooms . ' Bedrooms';
			// }
			// if(isset($item->Price)){
			// 	$data['price'][$item->Price]=$item->Price;
			// }

		
			if(isset($item->Price)){
				if( (int) $minPrice < $item->Price && (int) $maxPrice > $item->Price) {
					$data['items'][]=$item;
					
				}

				if(isset($item->neighbourhood)){
						$data['Neighbourhoods'][$item->neighbourhood]=$item->neighbourhood;		
					}	
				}

			
		}
		if($sort != 'all'){
			if($sort != 'asc_price'){
				//price asc
				usort($data['items'], function($a, $b) {
			    return $b->Price - $a->Price;
				});
			}
			if($sort != 'dsc_price'){

				//price asc
				usort($data['items'], function($a, $b) {
			    return $a->Price - $b->Price;
				});
			}


		//price dsc	
		}
			

		

	   // $items = parent::getItems();	

		return $data;
	}



		
	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 *
	 * @return void
	 */
	protected function loadFormData()
	{
		$app              = Factory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null)
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(Text::_("COM_ITEMLIST_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
	 *
	 * @param   string  $date  Date to be checked
	 *
	 * @return bool
	 */
	private function isValidDate($date)
	{
		$date = str_replace('/', '-', $date);
		return (date_create($date)) ? Factory::getDate($date)->format("Y-m-d") : null;
	}
}
