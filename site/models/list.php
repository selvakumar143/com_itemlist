<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Itemlist
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2021 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use \Joomla\CMS\Factory;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;
use \Joomla\CMS\Helper\TagsHelper;

/**
 * Itemlist model.
 *
 * @since  1.6
 */
class ItemlistModelList extends \Joomla\CMS\MVC\Model\ItemModel
{
	protected function populateState()
	{
		$app = Factory::getApplication();
		$params = $app->getParams();
		$this->setState("params", $params);
	}
}
