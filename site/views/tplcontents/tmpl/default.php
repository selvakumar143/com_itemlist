<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Itemlist
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2021 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Layout\LayoutHelper;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

$user       = Factory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_itemlist') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tplcontentform.xml');
$canEdit    = $user->authorise('core.edit', 'com_itemlist') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tplcontentform.xml');
$canCheckin = $user->authorise('core.manage', 'com_itemlist');
$canChange  = $user->authorise('core.edit.state', 'com_itemlist');
$canDelete  = $user->authorise('core.delete', 'com_itemlist');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_itemlist/css/itemlist.css');
		// usort($raw, function($a, $b) {
  //   		return $a['Price'] - $b['Price'];
  //   	});

?>
<div class="itemlist">

	<h1 class="font-weight-bold text-center p-5 mb-4">Property for Sales</h1>

	<div class="row form-group filter-group border-top border-bottom pt-4 pb-4">
			<div class="col-md-2 col-xs-12 p-2">
				 <select class=" form-control form-select form-select-lg mb-3"  name="bedrooms" id="bedrooms" onchange="FilterItems()">
				 	<?php foreach ($this->data['bedrooms'] as $i => $bedroom) : ?>
				    	<option value="<?php echo $i  ?>" <?php echo $this->data['query']['bedrooms'] == $i ? "selected" :"" ?> > <?php echo $bedroom ?> </option>
				    <?php endforeach ?>		
				  </select>
			</div>
				<div class="col-md-2 col-xs-12 p-2">
				 <select class="form-control form-select"  name="neighbourhoods" id="neighbourhoods" onchange="FilterItems()">
				    	<?php foreach ($this->data['Neighbourhoods'] as $i => $Neighbourhood) : ?>
				    	<option value="<?php echo $i ?>" <?php echo $this->data['query']['neighbourhood'] == $i ? "selected" :""  ?>> <?php echo $Neighbourhood ?> </option>
				    <?php endforeach ?>
		
				  </select>
			</div>	
			<div class="col-md-2 col-xs-12 p-2">
					<select class="form-control form-select"  name="minPrice" id="minPrice" onchange="FilterItems()">
				    	<?php foreach ($this->data['minPrice'] as $i => $price) : ?>
				    		<option value="<?php echo $i ?>" <?php echo $this->data['query']['minPrice'] == $i ? "selected" :""  ?>> <?php echo $price ?> </option>
				   	 <?php endforeach ?>
		
				  </select>
			</div>	
			<div class="col-md-2 col-xs-12 p-2">
				 <select class="form-control form-select"  name="maxPrice" id="maxPrice" onchange="FilterItems()">
				    	<?php foreach ($this->data['maxPrice'] as $i => $price) : ?>
				    		<option value="<?php echo $i ?>" <?php echo $this->data['query']['maxPrice'] == $i ? "selected" : ""  ?>> <?php echo $price ?> </option>
				   	 <?php endforeach ?>
				  </select>
			</div>	
			<div class="col-md-2 col-xs-12 p-2">
				 <select class=" form-control form-select"  name="sort" id="sort" onchange="FilterItems()">
				    <option value="all">Sort by</option>
				    <option value="asc_price" <?php echo $this->data['query']['sort'] == "asc_price" ? "selected" : ""  ?>>Price low to high</option>
				    <option value="dsc_price" <?php echo $this->data['query']['sort'] == "dsc_price" ? "selected" : ""  ?>>Price hight to low</option>	
				  </select>
			</div>
			<div class="col-md-2 col-xs-12 p-2 text-right">
				<h5 class="font-weight-bold"><?php echo  count($this->data['items']). ' Results' ?></h5>
			</div>
	</div>
	<div class="row"> 

		<?php foreach ($this->data['items'] as $i => $item) : ?>


			<div class="col-md-4 col-xs-12 mt-5">
				<div class="image_div overlay"><img src="<?php echo $item->Images[0]->url ?>" class="img-fluid list-image" alt="Responsive image">
				</div>
				<div class="text-center">
					<h4 class="title mt-5 text-uppercase text-muted" >

					<a class="text-muted font-weight-bold" href="<?php echo JRoute::_('index.php?option=com_itemlist&view=tplcontent&id='.$item->Slug); ?>">	<?php echo isset($item->Title) ? $item->Title : ""  ?>
					</a>


					</h4>					
					<h6 class="text-muted pt-3 ">
					 <?php echo $item->Bedrooms .' Bedroom ' . $item->Building_Type .' for  ' .  $item->Property_Type   ?> 

					</h6>

					<h4 class="price font-weight-bold mt-5"> <?php echo isset($item->Price) ? $item->Price ."€":"" ?> </h4>
				</div>
			</div>		
		
		<?php endforeach; 

$uri =   JFactory::getURI();
 $urlArr=explode("?",$uri);
	$baseURI=$urlArr[0];
		?>
	</div>
	
	
	
							
</div>
<style>

	.chzn-container-single .chzn-single{
		border:unset;
		border-radius:0px;
		background-color: #fff !important;
		box-shadow:none;
		background:unset;
	}
</style>
<script type="text/javascript">


	function FilterItems(){
			var bedrooms = document.getElementById("bedrooms").value;
			var neighbourhood = document.getElementById("neighbourhoods").value;
			var minPrice = document.getElementById("minPrice").value;
			var maxPrice = document.getElementById("maxPrice").value;
			var sort = document.getElementById("sort").value;
			var query='?_limit=12&';
			
			query= query+'&Bedrooms='+bedrooms;
			query= query+'&neighbourhood='+neighbourhood;
			query= query+'&min='+minPrice;
			query= query+'&max='+maxPrice;
			query= query+'&sort='+sort;

			window.location = "<?php echo $baseURI ?>"+ query;

			
			
	}

	
</script>



			