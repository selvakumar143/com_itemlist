<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Itemlist
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2021 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\HTML\HTMLHelper;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Uri\Uri;
use \Joomla\CMS\Router\Route;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Layout\LayoutHelper;

HTMLHelper::addIncludePath(JPATH_COMPONENT . '/helpers/html');
HTMLHelper::_('bootstrap.tooltip');
HTMLHelper::_('behavior.multiselect');
HTMLHelper::_('formbehavior.chosen', 'select');

$user       = Factory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_itemlist') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tplcontentform.xml');
$canEdit    = $user->authorise('core.edit', 'com_itemlist') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tplcontentform.xml');
$canCheckin = $user->authorise('core.manage', 'com_itemlist');
$canChange  = $user->authorise('core.edit.state', 'com_itemlist');
$canDelete  = $user->authorise('core.delete', 'com_itemlist');

// Import CSS
$document = Factory::getDocument();
$document->addStyleSheet(Uri::root() . 'media/com_itemlist/css/itemlist.css');

?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js"></script>





<div class="item_fields">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>





  <!-- Initialize Swiper -->

            <div class="container">
                <div class="d-flex justify-content-start row">
                    <div class="menu-image col-md-7" id="">


                         <img  id="zoom_03" src="<?php echo $this->item[0]->Images[0]->url ?>" class="img-fluid list-image" alt="Responsive image" data-zoom-image="<?php echo $this->item[0]->Images[0]->url ?>"> 
                         <br>

                               <!-- Swiper -->
                              <div class="swiper-container">
                                <div class="swiper-wrapper">

                                    <?php  $i=1; foreach($this->item[0]->Images as $image) {?>

                                  <div class="swiper-slide"><img id="img_<?php echo $i++; ?>"  src="<?php echo $image->url ?>" width="400"></div>

                                  
                                                         <?php } ?>

                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                              </div>



                    </div>
                    <!-- <div class="menu-image col-md-1"></div> -->
                    <div class="menu-text col-md-5 ">
                        <div class="share-section pb-5">
                            <div class="float-right"> <h3><i class="fas fa-share-alt pl-4"></i> <i class="pl-4 fa fa-heart" aria-hidden="true"></i></h3>
                                </div>
                        </div>
                        <div class="price-section border-top pt-4">
                            <h4 class="price "> <span class=" font-weight-bold ">€ <?php echo $this->item[0]->Price ?> </span>

                            <?php echo $this->item[0]->Bedrooms .' bed' ?>
                             | <?php echo $this->item[0]->Floor_Plans[0]->size .' sqm' ?> 

                        </h4>
                        <h6 class="sale-type mt-3 mb-2 text-muted">
                            <?php echo $this->item[0]->Bedrooms .' Bedroom ' . $this->item[0]->Building_Type .' for  ' .  $this->item[0]->Property_Type   ?> </h6>

                            <p class="pt-3 contact-link mb-5 "> <i class="fas fa-home"> </i>  Please contact us</p>
                            <button class="btn  mt-3 btn-dark btn-lg btn-block font-weight-bold">CONTACT AGENT</button>
                    </div>
                    <div class="fact-section pt-5">
                        <h3 class="border-bottom pb-3">FACTS & FEATURES</h3> 
                        
                        <h4 class="row pt-3"><span class="font-weight-bold col-md-5"> Neighbourhoods: </span> <span class="col-md-7 text-muted"> <?php echo $this->item[0]->neighbourhood ?>  </span></h4>  


                        <h4 class="row pt-3"><span class="font-weight-bold col-md-5">Price per sqm: </span> <span class="col-md-7 text-muted">€<?php echo $this->item[0]->Price_Per_Sqm ?> </span> </h4>  


                        <h4 class="row pt-3"><span class="font-weight-bold col-md-5">Brochure: </span> <span class="col-md-7 text-muted"><a href="<?php echo $this->item[0]->Brochure[0]->url ?>" target="__Blank" class="text-muted">Download</a> </span> </h4> 

                        <h4 class="row pt-3"> <span class="font-weight-bold col-md-5">Floor plan: </span> <span class="col-md-7 text-muted"><a href="<?php echo $this->item[0]->Floor_Plans[0]->url ?>" class="text-muted" target="__Blank">View</a> </span> </h4>  

                        <h5 class="description pt-5 text-muted ">
                            <?php echo $this->item[0]->Description ?>  
                        </h5> 
                    </div>
                    <div class="contact-detail mt-5">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo $this->item[0]->Negotiator->Image->url ?>" class="img-thumbnail list-image" alt="<?php echo $this->item[0]->Negotiator->Name ?>">
                            </div>
                            <div class="col-md-7 mt-5">
                                <h4 class="font-weight-bold"><?php echo $this->item[0]->Negotiator->Name ?></h4>
                                <h5><?php echo $this->item[0]->Negotiator->Phone ?></h5>
                                <h5><?php echo $this->item[0]->Negotiator->Email ?> | <a class="" href="mailto:<?php echo $this->item[0]->Negotiator->Email ?>">Email</a></h5>
                            </div>
                            <div id="map"></div>

                    </div>
                </div>
            </div>



<style>


  .com-itemlist .price span {
    font-size: 24px;
    color:black;
  }
  .com-itemlist .sale-type{
    font-size:13px;
  }

    .com-itemlist .contact-link  {
        color:#c89b57;
        font-weight:bold;
        font-size:13px;
        text-decoration:underline;


    }

 .com-itemlist .btn-dark{
    background-color: #000;
    height: 50px;
 }

    .com-itemlist .price  {
    color: #6c757d ;

  }

  #map {
        height: 100%;
      }

      /*set a border on the images to prevent shifting*/
#gallery_01 img {
    border: 2px solid white;
}

/*Change the colour*/
.active img {
    border: 2px solid #333 !important;
}


</style>
	

    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwXIfP1qn2b-_pJB7tjGs4GdCCKIfPALA&v=weekly"></script> -->
    <script>
  


 

//pass the images to Fancybox
$("#zoom_03").ezPlus();

    </script>

      <script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 2,
      spaceBetween: 10,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
  </script>



</div>

</div>