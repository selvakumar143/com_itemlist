<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Itemlist
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2021 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Factory;
use Joomla\CMS\Categories\Categories;

/**
 * Class ItemlistRouter
 *
 */
class ItemlistRouter extends RouterView
{
	private $noIDs;
	public function __construct($app = null, $menu = null)
	{
		$params = JComponentHelper::getComponent('com_itemlist')->params;
		$this->noIDs = (bool) $params->get('sef_ids');
		
		$tplcontents = new RouterViewConfiguration('tplcontents');
		$this->registerView($tplcontents);
			$tplcontent = new RouterViewConfiguration('tplcontent');
			$tplcontent->setKey('id')->setParent($tplcontents);
			$this->registerView($tplcontent);

		parent::__construct($app, $menu);

		$this->attachRule(new MenuRules($this));

		if ($params->get('sef_advanced', 0))
		{
			$this->attachRule(new StandardRules($this));
			$this->attachRule(new NomenuRules($this));
		}
		else
		{
			JLoader::register('ItemlistRulesLegacy', __DIR__ . '/helpers/legacyrouter.php');
			JLoader::register('ItemlistHelpersItemlist', __DIR__ . '/helpers/itemlist.php');
			$this->attachRule(new ItemlistRulesLegacy($this));
		}
	}


	
		/**
		 * Method to get the segment(s) for an tplcontent
		 *
		 * @param   string  $id     ID of the tplcontent to retrieve the segments for
		 * @param   array   $query  The request that is built right now
		 *
		 * @return  array|string  The segments of this item
		 */
		public function getTplcontentSegment($id, $query)
		{
			return array((int) $id => $id);
		}

	
		/**
		 * Method to get the segment(s) for an tplcontent
		 *
		 * @param   string  $segment  Segment of the tplcontent to retrieve the ID for
		 * @param   array   $query    The request that is parsed right now
		 *
		 * @return  mixed   The id of this item or false
		 */
		public function getTplcontentId($segment, $query)
		{
			return (int) $segment;
		}
}
